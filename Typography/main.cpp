

#include <GL\glew.h>
#include <GL\freeglut.h>
#include <iostream>

#include <iostream>
#include <fstream>
#include <vector>
#include "corona.h"

void drawFullString(char *string,float x,float y,float z);
void renderFullText(char *string);
void saveImage(int WIDTH,int HEIGHT,const char* imageName,GLvoid* imageData);
std::string getImageName(int i);
void drawStringPerCharacter(char *string,float x,float y,float z) 
{  
	char *c;
	char name[20] = {0};
	glColor4f(255,0,0,1);
	glRasterPos3f(x, y,z);
	int i=0;
	for (c=string; *c != '\0'; c++) 
	{
		i++;
		char idx[10];
		//itoa(i,idx,10);
		
		sprintf(name,"%02d",i);
		std::string imageName("test");
		imageName.append(std::string(name));
		imageName.append(".png");
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
		using namespace std;
		int WIDTH=500,HEIGHT=500;
		//std::vector<unsigned char> data(1000*1000*4);
		glReadBuffer(GL_BACK);
	
		//glReadPixels(0,0,1000,1000,GL_BGRA,GL_UNSIGNED_BYTE,&data[0]);
		GLvoid *imageData = malloc(WIDTH*HEIGHT*(4)); //Allocate memory for storing the image
		glReadPixels(0, 0, WIDTH, HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, imageData); //Copy the image to the array imageData
 


		//SaveImage((unsigned char*)&data);
		corona::Image *myImage = corona::CreateImage(WIDTH, HEIGHT, corona::PF_R8G8B8A8, imageData);
		corona::FlipImage(myImage, corona::CA_X); //Flip the image
		corona::SaveImage(imageName.c_str(), corona::FF_PNG, myImage); //Save it
	}
}

void drawStrokeText(char*string,int x,int y,int z)
{
	char *c;
	glPushMatrix();
	glTranslatef(x, y+8,z);
	for (c=string; *c != '\0'; c++)
	{
		glutStrokeCharacter(GLUT_STROKE_ROMAN , *c);
	}
	glPopMatrix();
}

void reshape(int w,int h) 
{ 
	glViewport(0,0,w,h); 
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity(); 
	gluOrtho2D(0,w,h,0); 
	glMatrixMode(GL_MODELVIEW); 
	glLoadIdentity(); 
}

// szPathName : Specifies the pathname
// lpBits	 : Specifies the bitmap bits
// w	: Specifies the image width
// h	: Specifies the image height

bool SaveImage(/*char* szPathName, */void* lpBits/*, int w, int h*/)

{ 

	//Create a new file for writing

	FILE *pFile = fopen("text.bmp", "wb");

	if(pFile == NULL)

	{ 

	return false;

	}
	int w = 1000;
	int h = 1000;
	BITMAPINFOHEADER BMIH;

	BMIH.biSize = sizeof(BITMAPINFOHEADER);

	BMIH.biSizeImage = w * h * 3;

	// Create the bitmap for this OpenGL context

	BMIH.biSize = sizeof(BITMAPINFOHEADER);

	BMIH.biWidth = w;

	BMIH.biHeight = h;

	BMIH.biPlanes = 1;

	BMIH.biBitCount = 24;

	BMIH.biCompression = BI_RGB;

	BMIH.biSizeImage = w * h* 3; 

	BITMAPFILEHEADER bmfh;

	int nBitsOffset = sizeof(BITMAPFILEHEADER) + BMIH.biSize; 

	LONG lImageSize = BMIH.biSizeImage;

	LONG lFileSize = nBitsOffset + lImageSize;

	bmfh.bfType = 'B'+('M'<<8);

	bmfh.bfOffBits = nBitsOffset;

	bmfh.bfSize = lFileSize;

	bmfh.bfReserved1 = bmfh.bfReserved2 = 0;

	//Write the bitmap file header

	UINT nWrittenFileHeaderSize = fwrite(&bmfh, 1, 

	sizeof(BITMAPFILEHEADER), pFile);

	//And then the bitmap info header

	UINT nWrittenInfoHeaderSize = fwrite(&BMIH, 

	1, sizeof(BITMAPINFOHEADER), pFile);

	//Finally, write the image data itself 

	//-- the data represents our drawing

	UINT nWrittenDIBDataSize = 

	fwrite(lpBits, 1, lImageSize, pFile);

	fclose(pFile);

 

return true;

}


void render(void)
{ 
	glClear(GL_COLOR_BUFFER_BIT); 
	glLoadIdentity();
	{
		drawFullString("Kinetic Typography Project",200,200,0);
		//drawStringPerCharacter("Kinetic Typography Project",200,200,0);
		//drawBitmapText("Mohammed Moiyadi ",200,200,0);
	}
	
	glutSwapBuffers(); 

} 

void drawFullString(char *string,float x,float y,float z)
{
	char name[20] = {0};
	
	for (int i=0;i<10;i++)
	{
		glRasterPos3f(x, y,z);
		// set color with opacity
		GLfloat opacity = (0.1*(i+1));
		glColor4f(255,0,0,opacity);
		std::string imageName = getImageName(i);
		renderFullText(string);
		using namespace std;
		int WIDTH=500,HEIGHT=500;
		glReadBuffer(GL_BACK);
		GLvoid *imageData = malloc(WIDTH*HEIGHT*(4)); //Allocate memory for storing the image
		glReadPixels(0, 0, WIDTH, HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, imageData); //Copy the image to the array imageData
		saveImage(WIDTH,HEIGHT,imageName.c_str(),imageData);
	}
	for (int i=10;i<20;i++)
	{
		//glRasterPos3f(x, y,z);
		//glColor4f(255,0,0,1);
		std::string imageName = getImageName(i);
		//renderFullText(string);
		using namespace std;
		int WIDTH=500,HEIGHT=500;
		glReadBuffer(GL_BACK);
		GLvoid *imageData = malloc(WIDTH*HEIGHT*(4)); //Allocate memory for storing the image
		glReadPixels(0, 0, WIDTH, HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, imageData); //Copy the image to the array imageData
		saveImage(WIDTH,HEIGHT,imageName.c_str(),imageData);
	}
	int j=0;
	for (int i=20;i<30;i++)
	{
		glRasterPos3f(x, y,z);
		// set color with opacity
		GLfloat opacity = (1 - 0.1*(j+1));
		j++;
		glColor4f(255,0,0,opacity);
		std::string imageName = getImageName(i);
		renderFullText(string);
		using namespace std;
		int WIDTH=500,HEIGHT=500;
		glReadBuffer(GL_BACK);
		GLvoid *imageData = malloc(WIDTH*HEIGHT*(4)); //Allocate memory for storing the image
		glReadPixels(0, 0, WIDTH, HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, imageData); //Copy the image to the array imageData
		saveImage(WIDTH,HEIGHT,imageName.c_str(),imageData);
	}

}

std::string getImageName(int i)
{
	char name[20] = {0};
	sprintf(name,"%02d",i);
	std::string imageName("test");
	imageName.append(std::string(name));
	imageName.append(".png");
	return imageName;
}

void renderFullText(char *string)
{
	char *c;
	for (c=string; *c != '\0'; c++) 
	{
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,*c);
	}
}

void saveImage(int WIDTH,int HEIGHT,const char* imageName,GLvoid* imageData)
{
	corona::Image *myImage = corona::CreateImage(WIDTH, HEIGHT, corona::PF_R8G8B8A8, imageData);
	corona::FlipImage(myImage, corona::CA_X); //Flip the image
	corona::SaveImage(imageName, corona::FF_PNG, myImage); //Save it
}

// Main routine
// Set up OpenGL, hook up callbacks, and start the main loop
int main( int argc, char** argv )
{
	// Need to double buffer for animation
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );

	// Create and position the graphics window
	glutInitWindowPosition( 0, 0 );
	glutInitWindowSize( 600, 360 );
	glutCreateWindow( "Welcome to Open Gl" );
	glutDisplayFunc(render);
	//glutIdleFunc(render);
	glutReshapeFunc(reshape); 
	// Start the main loop.  glutMainLoop never returns.
	glutMainLoop(  );

	return(0);          // Compiler requires this to be here. (Never reached)
}