#include <SFML/Graphics.hpp>
#include <string>
#include "Effect.hpp"
long double count = 0 ;
#include <vector>
#include <cmath>
const sf::Font* Effect::s_font = NULL;

class WaveBlur : public Effect
{
public:

	WaveBlur() :
	  Effect("wave + blur")
	  {
	  }

	  bool onLoad()
	  {
		  // Create the text
		  m_text.setString("Hello World");
		  m_text.setFont(getFont());
		  m_text.setCharacterSize(22);
		  m_text.setPosition(200, 400);
		  // Load the shader
		  if (!m_shader.loadFromFile("resources/wave.vert", "resources/blur.frag"))
			  return false;

		  return true;
	  }

	  void onUpdate(float time, float x, float y)
	  {
		  m_shader.setParameter("wave_phase", time);
		  m_shader.setParameter("wave_amplitude", x * 40, y * 40);
		  m_shader.setParameter("blur_radius", (x + y) * 0.008f);
	  }

	  void onDraw(sf::RenderTarget& target, sf::RenderStates states) const
	  {
		  states.shader = &m_shader;
		  target.draw(m_text, states);
	  }

private:

	sf::Text m_text;
	sf::Shader m_shader;
};



int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Shader",
		sf::Style::Titlebar | sf::Style::Close);
	window.setVerticalSyncEnabled(true);
	sf::Font font;
	if (!font.loadFromFile("resources/sansation.ttf"))
		return EXIT_FAILURE;
	Effect::setFont(font);
	std::vector<Effect*> effects;
	effects.push_back(new WaveBlur);
	std::size_t current = 0;

	// Initialize them
	for (std::size_t i = 0; i < effects.size(); ++i)
		effects[i]->load();

	// Create the messages background
	sf::Texture textBackgroundTexture;
	if (!textBackgroundTexture.loadFromFile("resources/text-background.png"))
		return EXIT_FAILURE;
	sf::Sprite textBackground(textBackgroundTexture);
	textBackground.setPosition(0, 520);
	textBackground.setColor(sf::Color(255, 255, 255, 200));

	// Create the description text
	sf::Text description("Current effect: " + effects[current]->getName(), font, 20);
	description.setPosition(10, 530);
	description.setColor(sf::Color(80, 80, 80));
	sf::Clock clock;
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}		
		float x = static_cast<float>(sf::Mouse::getPosition(window).x) / window.getSize().x;
		float y = static_cast<float>(sf::Mouse::getPosition(window).y) / window.getSize().y;
		effects[current]->update(clock.getElapsedTime().asSeconds(), x, y);
		sf::Image img(window.capture());
		count++;
		std::string str = "Image" + std::to_string(count) + ".tga";
		img.saveToFile(str);
		if(clock.getElapsedTime().asSeconds() > 10 )
			window.close();
		window.setFramerateLimit(20);		
		window.clear(sf::Color(255, 128, 0));
		window.draw(*effects[current]);
		window.display();
	}
	return 0;
}